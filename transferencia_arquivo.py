from smq_client import SmqCliente
import os

FILA_ARQUIVOS = 'arquivos' 
QUANTIDADE_RECEBE = 10

def envia_todos_arquivos( diretorio ):
    sc = SmqCliente()
    for path ,_, files in os.walk( diretorio ):
        for file in files:
            sc.envia(FILA_ARQUIVOS, mensagem=os.path.join(path, file) )

def limpa_envia_todos_arquivos():
    r = sc.recebe_nao_bloqueante_commit(FILA_ARQUIVOS)
    while r.existe():     
        r = sc.recebe_nao_bloqueante_commit(FILA_ARQUIVOS)
    print("b")


def recebe_todos_arquivo_lista():
    sc = SmqCliente()
    retorno = list()
    r = sc.recebe_nao_bloqueante_commit(FILA_ARQUIVOS)
    while r.existe() and QUANTIDADE_RECEBE > len( retorno ):     
        print( len( retorno ) , r )
        var = r.mensagem()
        retorno.append( var )

        r = sc.recebe_nao_bloqueante_commit(FILA_ARQUIVOS)
        print( len( retorno ))

        
